#pragma once
#include "Test.h"

namespace std
{
	bool Test::spellTest(tm* datetime, string awaitedResult, string& testResultMessage)
	{
		string result = DateSpeller::spell(mktime(datetime));
		testResultMessage = "";
		testResultMessage.append("��������� ������:\r\n")
			.append(awaitedResult + "\r\n")
			.append("�������� ������:\r\n")
			.append(result + "\r\n");
		return awaitedResult.compare(result) == 0;
	}
}
