#pragma once
#include "DateSpeller.h"
#include <ctime>

namespace std
{
	class Test
	{
	public:
		static bool spellTest(tm* datetime, string awaitedResult, string& testResultMessage);
	};
}
