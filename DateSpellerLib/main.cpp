#include "Test.h"
#include <clocale>
#include <cstdio>
#include <ctime>
#include <string>
#include <iostream>
#include <windows.h>
#include <stdlib.h>

void initTime(tm* datetime, int day, int month, int year)
{
	datetime->tm_year = year - 1900;
	datetime->tm_mon = month-1;
	datetime->tm_mday = day;
}

bool runTest(tm* datetime, std::string expectedString, int testnumber)
{
	std::string testinfo = "";
	bool testResult = std::Test::spellTest(datetime, expectedString, testinfo);
	if (testResult)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x2F);
		std::cout << "====================" << std::endl << "|| Test " << testnumber << " succeed ||" << std::endl << "====================" << std::endl;
	}
	else
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x4F);
		std::cout << "===================" << std::endl << "|| Test " << testnumber << " failed ||" << std::endl << "===================" << std::endl;
	}
	std::cout << testinfo << std::endl << std::endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
	return testResult;
}

void main()
{
	setlocale(LC_ALL, "Russian");
	//init testing
	time_t now = time(nullptr);
	tm* datetime = new tm;
	localtime_s(datetime, &now);
	int totalTests = 0, succeedTests = 0;

	//Test 01.01.2000
	initTime(datetime, 1, 1, 2000);
	if (runTest(datetime, "������ ������ ������������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2001
	initTime(datetime, 1, 1, 2001);
	if (runTest(datetime, "������ ������ ��� ������ ������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2010
	initTime(datetime, 1, 1, 2040);
	if (runTest(datetime, "������ ������ ��� ������ ���������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2100
	initTime(datetime, 1, 1, 2200);
	if (runTest(datetime, "������ ������ ��� ������ ���������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2101
	initTime(datetime, 1, 1, 2402);
	if (runTest(datetime, "������ ������ ��� ������ ��������� ������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2100
	initTime(datetime, 1, 1, 2890);
	if (runTest(datetime, "������ ������ ��� ������ ��������� ����������� ����", ++totalTests))
		succeedTests++;

	//Test 01.01.2100
	initTime(datetime, 1, 1, 2345);
	if (runTest(datetime, "������ ������ ��� ������ ������ ����� ������ ����", ++totalTests))
		succeedTests++;

	//Test 01.01.1999
	initTime(datetime, 1, 1, 1999);
	if (runTest(datetime, "������ ������ ���� ������ ��������� ��������� �������� ����", ++totalTests))
		succeedTests++;

	//Test 10.07.2000
	initTime(datetime, 10, 7, 2000);
	if (runTest(datetime, "������� ���� ������������� ����", ++totalTests))
		succeedTests++;

	//Test 11.12.2000
	initTime(datetime, 11, 12, 2000);
	if (runTest(datetime, "������������ ������� ������������� ����", ++totalTests))
		succeedTests++;

	//Test 11.12.2000
	initTime(datetime, 20, 03, 2000);
	if (runTest(datetime, "��������� ����� ������������� ����", ++totalTests))
		succeedTests++;

	//Test 11.12.2000
	initTime(datetime, 29, 06, 2000);
	if (runTest(datetime, "�������� ������� ���� ������������� ����", ++totalTests))
		succeedTests++;

	//Test 11.12.2000
	initTime(datetime, 30, 11, 2000);
	if (runTest(datetime, "��������� ������ ������������� ����", ++totalTests))
		succeedTests++;

	//Test 11.12.2000
	initTime(datetime, 31, 05, 2000);
	if (runTest(datetime, "�������� ������ ��� ������������� ����", ++totalTests))
		succeedTests++;


	std::cout << "Tests successful: " << succeedTests << "\\" << totalTests << std::endl;
	getchar();
}
