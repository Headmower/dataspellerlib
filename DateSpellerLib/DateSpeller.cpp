﻿#pragma once
#include "DateSpeller.h"
#include <ctime>

namespace std
{
	string DateSpeller::spell(time_t date)
	{
		string result;
		tm* ptm = new tm; // Sergey: no matching delete found, this will leak memory.
		localtime_s(ptm, &date); // Sergey: error code not checked. Try spell(-1).
		//char* buffer = new char[32];
		//strftime(buffer, 32, "%d %m %Y", ptm);
		result.append(parseNumberImenSred(ptm->tm_mday));
		result.append(" ");
		result.append(parseMonthRod(ptm->tm_mon));
		result.append(" ");
		result.append(parseNumberRod(ptm->tm_year+1900));
		result.append(" года");
		return result;
	}

	//NOT IMPLEMENTED
	void DateSpeller::spellNumber(int number,string& result, EPadezh padezh = Imenitelny, int depth = 0)
	{
		throw exception("Not implemented"); // Sergey: This is not portable. It will not compile in gcc e.g.
		if (number / 1000 / depth == 0)
			return;
		string strbuffer;
		spellNumber(number / 1000, result, padezh, depth++);
		//case
	}

	string DateSpeller::parseNumberImenSred(int number)
	{
		string result;
		string strbuffer;
		int tens = number / 10;
		int ones = number % 10;
		if(tens < 0 || tens > 3 || (tens == 0 && ones == 0))
			throw exception("Date Format exception");
		if(ones == 0)
		{
			switch(tens)
			{
			case 1:
				strbuffer = "десятое";
				break;
			case 2:
				strbuffer = "двадцатое";
				break;
			case 3:
				strbuffer = "тридцатое";
				break;
			}
			result.append(strbuffer);
		}
		else if(tens == 1)
		{
			switch(ones)
			{
			case 1:
				strbuffer = "одиннадцатое";
				break;
			case 2:
				strbuffer = "двенадцатое";
				break;
			case 3:
				strbuffer = "тринадцатое";
				break;
			case 4:
				strbuffer = "четырнадцатое";
				break;
			case 5:
				strbuffer = "пятнадцатое";
				break;
			case 6:
				strbuffer = "шестнадцатое";
				break;
			case 7:
				strbuffer = "семнадцатое";
				break;
			case 8:
				strbuffer = "восемнадцатое";
				break;
			case 9:
				strbuffer = "девятнадцатое";
				break;
			}
			result.append(strbuffer);
		}
		else
		{
			result.append(switchTens(tens));
			switch (ones)
			{
			case 1:
				strbuffer = "первое";
				break;
			case 2:
				strbuffer = "второе";
				break;
			case 3:
				strbuffer = "третье";
				break;
			case 4:
				strbuffer = "четвёртое";
				break;
			case 5:
				strbuffer = "пятое";
				break;
			case 6:
				strbuffer = "шестое";
				break;
			case 7:
				strbuffer = "седьмое";
				break;
			case 8:
				strbuffer = "восьмое";
				break;
			case 9:
				strbuffer = "девятое";
				break;
			}
			result.append(strbuffer);
		}
		return result;
	}

	string DateSpeller::parseMonthRod(int month)
	{
		switch(month)
		{
		case 0:
			return "января";
		case 1:
			return "февраля";
		case 2:
			return "марта";
		case 3:
			return "апреля";
		case 4:
			return "мая";
		case 5:
			return "июня";
		case 6:
			return "июля";
		case 7:
			return "августа";
		case 8:
			return "сентября";
		case 9:
			return "октября";
		case 10:
			return "ноября";
		case 11:
			return "декабря";
		default:
			throw exception("Month integer invalid");
		}
	}

	string DateSpeller::switchThousands(int number)
	{
		switch (number)
		{
		case 1:
			return "одна тысяча ";			
		case 2:
			return "две тысячи ";
		case 3:
			return "три тысячи ";
		case 4:
			return "четыре тысячи ";
		case 5:
			return "пять тысяч ";
		case 6:
			return "шесть тысяч ";
		case 7:
			return "семь тысяч ";
		case 8:
			return "восемь тысяч ";
		case 9:
			return "девять тысяч ";
		default:
			return ""; // I would throw an error here. Which is what parseMonthRod does BTW.
		}
	}

	string DateSpeller::switchHundreds(int number)
	{
		switch(number)
		{
		case 1:
			return "сто ";
		case 2:
			return "двести ";
		case 3:
			return "триста ";
		case 4:
			return "четыреста ";
		case 5:
			return "пятьсот";
		case 6:
			return "шестьсот ";
		case 7:
			return "семьсот ";
		case 8:
			return "восемьсот ";
		case 9:
			return "девятьсот ";
		default:
			return "";
		}
	}

	string DateSpeller::switchTens(int number)
	{
		switch (number)
		{
		case 2:
			return "двадцать ";
		case 3:
			return "тридцать ";
		case 4:
			return "сорок ";
		case 5:
			return "пятьдесят ";
		case 6:
			return "шестьдесят ";
		case 7:
			return "семьдесят ";
		case 8:
			return "восемьдесят ";
		case 9:
			return "девяносто ";
		default:
			return "";
		}
	}

	string DateSpeller::parseNumberRod(int number)
	{
		string result;
		string strbuffer;
		int thousands = number / 1000;
		int hundreds = number % 1000 / 100;
		int tens = number % 100 / 10;
		int ones = number % 10;
		if (ones == 0)
		{
			if (tens == 0)
			{
				if (hundreds == 0)
				{
					switch (thousands)
					{
					case 0:
						strbuffer = "нулевого"; 
						break;
					case 1:
						strbuffer = "однотысячного";
						break;
					case 2:
						strbuffer = "двухтысячного";
						break;
					case 3:
						strbuffer = "трёхтысячного";
						break;
					case 4:
						strbuffer = "четырёхтысячного";
						break;
					case 5:
						strbuffer = "пятитысячного";
						break;
					case 6:
						strbuffer = "шеститысячного";
						break;
					case 7:
						strbuffer = "семитысячного";
						break;
					case 8:
						strbuffer = "восьмитысячного";
						break;
					case 9:
						strbuffer = "девятитысячного";
						break;
					}
					result.append(strbuffer);
				}
				else
				{
					result.append(switchThousands(thousands));

					switch (hundreds)
					{
					case 1:
						strbuffer = "сотого";
						break;
					case 2:
						strbuffer = "двухсотого";
						break;
					case 3:
						strbuffer = "трёхсотого";
						break;
					case 4:
						strbuffer = "четырёхсотого";
						break;
					case 5:
						strbuffer = "пятисотого";
						break;
					case 6:
						strbuffer = "шестисотого";
						break;
					case 7:
						strbuffer = "семисотого";
						break;
					case 8:
						strbuffer = "восьмисотого";
						break;
					case 9:
						strbuffer = "девятисотого";
						break;
					}
					result.append(strbuffer);
				}
			}
			else
			{
				result.append(switchThousands(thousands));
				result.append(switchHundreds(hundreds));
				switch (tens)
				{
				case 1:
					strbuffer = "десятого";
					break;
				case 2:
					strbuffer = "двадцатого";
					break;
				case 3:
					strbuffer = "тридцатого";
					break;
				case 4:
					strbuffer = "сорокового";
					break;
				case 5:
					strbuffer = "пятидесятого";
					break;
				case 6:
					strbuffer = "шестидесятого";
					break;
				case 7:
					strbuffer = "семидесятого";
					break;
				case 8:
					strbuffer = "восьмидесятого";
					break;
				case 9:
					strbuffer = "девяностого";
					break;
				}
				result.append(strbuffer);
			}
		}
		else
		{
			result.append(switchThousands(thousands))
				.append(switchHundreds(hundreds));
			if (tens == 1)
			{
				switch (ones) {
				case 1:
					strbuffer = "одиннадцатого";
					break;
				case 2:
					strbuffer = "двенадцатого";
					break;
				case 3:
					strbuffer = "тринадцатого";
					break;
				case 4:
					strbuffer = "четырнадцатого";
					break;
				case 5:
					strbuffer = "пятнадцатого";
					break;
				case 6:
					strbuffer = "шестнадцатого";
					break;
				case 7:
					strbuffer = "семнадцатого";
					break;
				case 8:
					strbuffer = "восемнадцатого";
					break;
				case 9:
					strbuffer = "девятнадцатого";
					break;
				}
			}
			else
			{
				result.append(switchTens(tens));
				switch (ones)
				{
				case 1:
					strbuffer = "первого";
					break;
				case 2:
					strbuffer = "второго";
					break;
				case 3:
					strbuffer = "третьего";
					break;
				case 4:
					strbuffer = "четвёртого";
					break;
				case 5:
					strbuffer = "пятого";
					break;
				case 6:
					strbuffer = "шестого";
					break;
				case 7:
					strbuffer = "седьмого";
					break;
				case 8:
					strbuffer = "восьмого";
					break;
				case 9:
					strbuffer = "девятого";
					break;
				}
			}
			result.append(strbuffer);
		}
		return result;
	}
}
