﻿#pragma once
#include <string.h>
#include <string>

enum EPadezh
{
	Imenitelny,
	Roditelny,
	Datelny,
	Vinitelny,
	Tvoritelny,
	Predlozhny
};

enum ERod
{
	Muzhskoy,
	Zhensky,
	Sredny
};

enum EChislo
{
	Edinstvennoe,
	Mnozhestvennoe
};

namespace std // I woudn't put this into std; std is for the standard C++ library.
{
	class DateSpeller
	{
	public:
		static string spell(time_t date);
		static void spellNumber(int number,string& result, EPadezh padezh, int depth);
		static string parseNumberImenSred(int number);
		static string parseMonthRod(int month);
		static string switchThousands(int number);
		static string switchHundreds(int number);
		static string switchTens(int number);
		static string parseNumberRod(int number);
	};
}